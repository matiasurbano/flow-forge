import api from "@forge/api";
import {
  getIssueStoredCard,
  getAllCardsByStatus
} from './store'

export const getIssueComments = async(issueId) => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/issue/${issueId}/comment`);

  const data = await res.json();
  return data.comments.map(x => x.body);
}

export const getIssue = async(issueId) => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/issue/${issueId}?expand=`);

  const data = await res.json();
  return data;
}

export const getWorkflows = async() => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/workflow/search`);

  const data = await res.json();
  return data.values.map(x => x.id.name);
}

export const getIssueTransitions = async(issueId) => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/issue/${issueId}/transitions`);

  const data = await res.json();
  return data.transitions;
}

// const getWorkflowStatuses = async() => {
//   const res = await api
//     .asApp()
//     .requestJira(`/rest/api/3/status`);

//   const data = await res.json();
//   console.log("## getWorkflowStatuses: ",  JSON.stringify(data , null, 3));
//   return data;
// }


export const getProjectStatuses = async(projectId) => {
  const res = await api
    .asApp()
    .requestJira(`/rest/api/3/project/${projectId}/statuses`);

  const data = await res.json();
  // console.log("## projectStatuses: ",  JSON.stringify(data , null, 3));
  return data;
}

// const getAllDashboards = async() => {
//   const res = await api
//     .asApp()
//     .requestJira(`/rest/api/3/dashboard`);

//   const data = await res.json();
//   console.log("## DASHBOARD: ", JSON.parse(JSON.stringify(data)));
//   return data;
// }


export const sendEmailToAssignee = async (issueKey, notifyBody) => {
  const body = {
    htmlBody: notifyBody,
    subject: "Issue Health Monitor",
    to: {
      voters: false,
      watchers: false,
      groups: [
        {
          name: "jira-software-users"
        }
      ],
      reporter: false,
      assignee: true,
      users: []
    },
    restrict: {
      permissions: [],
      groups: []
    }
  };
  const response = await api
    .asUser()
    .requestJira(`/rest/api/3/issue/${issueKey}/notify`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(body)
    });
};