import { 
  addIssueStoredCardList,
  updateIssueStoredCardList,
  deleteIssueStoredCardList,
  getIssueStoredCard,
  getIssueStoredCardTodos
} from "./store";

export async function addCardTodo(issueId, statusId, todo) {
  const dateCreated = +new Date();
  todo.id = `${statusId}-${dateCreated.toString()}`
  todo.dateCreated = dateCreated;
  todo.isCompleted = false;
  return await addIssueStoredCardList(issueId, statusId, todo);
}

export async function toggleCardTodo(issueId, statusId, todoId) {
  return await updateIssueStoredCardList(issueId, statusId, todoId);
}

export async function deleteCardTodo(issueId, statusId, todoId) {
  return await deleteIssueStoredCardList(issueId, statusId, todoId);
}

export async function getCard(issueId, statusId) {
  return await getIssueStoredCard(issueId, statusId);
}

export async function getCardTodos(issueId, statusId) {
  return await getIssueStoredCardTodos(issueId, statusId);
}