import { store } from '@forge/api'


export async function getAllCardsByStatus(issueId, statuses) {
  const respPromise = statuses.map(status => {
    return store.onJiraIssue(issueId).get(`${statusId}-todos`);
  });
  return await Promise.all(respPromise);
}

export async function addIssueStoredCardList(issueId, statusId, todo) {
  const cardTodos = `${statusId}-todos`;

  let todos = await store.onJiraIssue(issueId).get(cardTodos) || {
    list: []
  };
  todos.list.push(todo)
  await store.onJiraIssue(issueId).set(cardTodos, todos);
  return todos;
}

export async function deleteIssueStoredCardList(issueId, statusId, todoId) {
  const cardTodos = `${statusId}-todos`;
  let todos = await store.onJiraIssue(issueId).get(cardTodos);
  const newTodos = todos.list.filter(x => x.id !== todoId)
  todos.list = newTodos;
  await store.onJiraIssue(issueId).set(cardTodos, todos);
  return todos;
}

export async function updateIssueStoredCardList(issueId, statusId, todoId) {
  try {
    const cardTodos = `${statusId}-todos`;
    let todos = await store.onJiraIssue(issueId).get(cardTodos);
    const selectedTodoIndex = todos.list.findIndex(x => x.id === todoId);
    todos.list[selectedTodoIndex].isCompleted = !todos.list[selectedTodoIndex].isCompleted;
    await store.onJiraIssue(issueId).set(cardTodos, todos);

    // CHECK ACTIONS and TRiggers
    return todos;
  } catch(err) {
    return null
  }

}

export async function getIssueStoredCard(issueId, statusId) {
  const cardKey = `${statusId}-card`;
  const card = await store.onJiraIssue(issueId).get(cardKey);
  if (card) {
    return card
  }
  return null;
}

export async function getIssueStoredCardTodos(issueId, statusId) {
  const cardTodos = `${statusId}-todos`;
  const todos = await store.onJiraIssue(issueId).get(cardTodos);
  if (todos) {
    return todos
  }
  return null;
}
