import ForgeUI, { 
  render, 
  Fragment, 
  Text, 
  TextArea, 
  IssuePanel,
  Form,
  Select,
  Option,
  Table, Head, Row, Cell,
  TextField,
  UserPicker,
  DatePicker,
  RadioGroup, 
  Radio,
  Button,
  ModalDialog,
  useState,
  useAction,
  useProductContext
} from '@forge/ui';

export default ({ handler, setCardOpen }) => { 
  return (
    <Fragment>
      <ModalDialog header="Add Activity" onClose={() => setCardOpen(false)}>
        <Form onSubmit={handler}>
          <TextField 
            name="title"
            label="Title" 
            isRequired
          />
          <TextArea 
            name="description" 
            label="Description"
          />
          <DatePicker 
            name="dueDate" 
            label="Due Date" 
          />
          <UserPicker name="assignee" label="Assign to" />
        </Form>
      </ModalDialog>
    </Fragment>
  );
}