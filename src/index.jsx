import ForgeUI, { 
  render, 
  Fragment, 
  Text, 
  StatusLozenge,
  DateLozenge,
  IssuePanel,
  Form,
  Select,
  Option,
  Table, Head, Row, Cell,
  SectionMessage,
  TextField,
  UserPicker,
  RadioGroup, 
  Radio,
  Button,
  ButtonSet,
  CheckboxGroup,
  Checkbox,
  AvatarStack,
  Avatar,
  ModalDialog,
  useState,
  useAction,
  useProductContext
} from '@forge/ui';
import api from "@forge/api";
import {
  getIssue,
  getIssueTransitions
 } from "./services/jira";
import {
  addCardTodo,
  toggleCardTodo,
  deleteCardTodo,
  getCard,
  getCardTodos
 } from "./services/card";
 import Card from "./components/Card";

const App = () => {
  const context = useProductContext();

  const [issue] = useState(async () => await getIssue(context.platformContext.issueKey));
  const { 
    key, 
    fields: { 
      issuetype,
      status,
      project: {name: projectName}
    },
  } = issue;

  const [transitions] = useState(async () => await getIssueTransitions(context.platformContext.issueId));
  const transitionsList = transitions.map(transition => {
    return {
      id: transition.to.id,
      name: transition.to.name
    }
  });
  
  const statusesSelect = transitionsList.map(transition => (
    <Option 
    defaultSelected={transition.id === status.id} 
    label={transition.name} 
    value={transition.id} />
    ))
    
  const [card, setCard] = useState(async () => await getCard(context.platformContext.issueId,  status.id));
  const [todos, setTodos] = useState(async () => await getCardTodos(context.platformContext.issueId,  status.id));
  const [isCardOpen, setCardOpen] = useState(false);

  const [todoSubmited, setTodoSubmited] = useAction(
    async (_, todoData) => {
      const newTodos = await addCardTodo(context.platformContext.issueId, status.id, todoData);
      setTodos(newTodos)
      setCardOpen(false);
      return todoData
    },
    undefined
  );

  const [todoDeleted, setTodoDeleted] = useAction(
    async (_, todoId) => {
      const newTodos = await deleteCardTodo(context.platformContext.issueId, status.id, todoId);
      setTodos(newTodos)
      return todoId
    },
    undefined
  );

  const [toggleTodo, setToggleTodo] = useAction(
    async (_, todoId) => {
      const newTodos = await toggleCardTodo(context.platformContext.issueId, status.id, todoId);
      setTodos(newTodos)
      return todoId
    },
    undefined
  );

  const openCard = (handler, isCardOpen, setCardOpen) => {
    return isCardOpen ?  (<Card 
      handler={handler} 
      setCardOpen={setCardOpen}
      />) : null;
    }
    
  const renderTodosSection = () => {
    const renderedTodos = todos && todos.list && todos.list.length > 0 ? (
      <Fragment>
        <Form submitButtonText="Add Activity"
              onSubmit={() => {setCardOpen(true)}}>
          <Table>
            <Head>
              <Cell>
                <Text content="Status" />
              </Cell>
              <Cell>
                <Text content="Title" />
              </Cell>
              <Cell>
                <Text content="Description" />
              </Cell>
              <Cell>
                <Text content="Supporter" />
              </Cell>
              <Cell>
                <Text content="Delete" />
              </Cell>
            </Head>
              { todos.list.map((todo) => (
                <Row>
                  <Cell>
                      <Button 
                        text={todo.isCompleted ? '✅' : '⬜️'}
                        onClick={() => {setToggleTodo(todo.id)}}
                      />
                  </Cell>
                  <Cell>
                    <Text content={todo.title} />
                  </Cell>
                  <Cell>
                    <Text content={todo.description} />
                  </Cell>
                  <Cell>
                    {todo.assignee && (
                      <AvatarStack>
                        <Avatar accountId={todo.assignee} />
                      </AvatarStack>
                    )}
                  </Cell>
                  <Cell>
                    <Button 
                      text="🗑"
                      onClick={() => {setTodoDeleted(todo.id)}}
                    />

                  </Cell>
                </Row>
              ))}
          </Table>
        </Form>
      </Fragment>
    ) : ( 
      <Fragment>
        <Button text="Add Activity" onClick={() => setCardOpen(true)} />
        <Text>*There are not activities in the checklist.*</Text>
      </Fragment>
    )
    
    return (
      <Fragment>
        <Text>**📝 Checklist** </Text>
        {openCard(setTodoSubmited, isCardOpen, setCardOpen)}
        {renderedTodos}
      </Fragment>
    )
  }

  return (
    <Fragment>
      {renderTodosSection()}
    </Fragment>
  );
};

export const run = render(
  <IssuePanel>
    <App />
  </IssuePanel>
);
