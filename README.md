# FLOW For Jira

![FLOW for Jira](./assets/flow-logo.png "FLOW for Jira")

**Flow** helps teams to simplify their daily work activities, creating a friendly and easy to use checklist to follow the progress of the story at anytime, it will allow them to create a independent checklist for each Status/Board Column.
**Flow** allows teams to easily extend functionality to adjust to different team needs.
WebHooks, Notifications and Workflow Validation are in the roadmap.

Developed by Matias Ariel Urbano

## Instructions

- Build and deploy your app by running:
```
forge deploy
```

- Install your app in an Atlassian site by running:
```
forge install
```

- Develop your app by running `forge tunnel` to proxy invocations locally:
```
forge tunnel
```

### Repository

Open Source hosted in Bitbucket

https://bitbucket.org/matiasurbano/flow-forge/src/master/